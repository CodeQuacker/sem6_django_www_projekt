FROM python:3.9

ENV DATE=$(date+'%Y-%m-%d')
ARG CACHEBUST=${DATE}

ENV PYTHONUNBUFFERED 1

RUN mkdir /app
WORKDIR /app

COPY requirements.txt /app/
RUN pip install -r requirements.txt

COPY . /app/
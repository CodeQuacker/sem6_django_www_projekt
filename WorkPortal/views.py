import re

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone

from WorkPortal.forms import UserCreationForm, OfferForm, ResponseForm, generate_response_form
from .mailer import Mailer, JobOfferResponse
from .models import JobOffer, ResponsesCounter, JobResponse


def home(request):
    categories = JobOffer._meta.get_field('category').choices

    return render(request, 'home.html', {
        'categories': categories
    })


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            # log the user in
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    helper = FormHelper()
    helper.form_method = 'post'
    helper.add_input(Submit('submit', 'Register', css_class='btn-primary'))
    form.helper = helper
    return render(request, 'registration.html', {'form': form})


@login_required
def create_job_offer(request):
    # TODO dorobić przenoszenie do świerzo utworznoego ogłoszenia po utworzeniu
    form = OfferForm(request.POST or None)
    context = {'form': form}
    if form.is_valid():
        job_offer = form.save(commit=False)
        job_offer.user_id = request.user
        job_offer.expiration_date = form.cleaned_data['expiration_date']
        job_offer.save()

        # Create placeholder response form
        JobResponse(job_offer_id_id=job_offer.id,
                    link=None,
                    email=True,
                    cv=True,
                    letter_of_motivation=True,
                    expected_wage=True
                    ).save()
        # Create counter
        ResponsesCounter(job_offer_id=job_offer, count=0).save()

        return redirect('create_response_form', job_offer.id)
    context['form'] = form

    return render(request, "createJobOffer.html", context)


@login_required
def create_response_form(request, job_offer_id):
    job_offer = get_object_or_404(JobOffer, id=job_offer_id)
    job_response = JobResponse.objects.get(job_offer_id_id=job_offer_id)
    form = ResponseForm(request.POST or None)
    context = {'form': form}
    if form.is_valid():
        form = form.save(commit=False)
        job_response.link = form.link
        job_response.email = form.email
        job_response.cv = form.cv
        job_response.letter_of_motivation = form.letter_of_motivation
        job_response.expected_wage = form.expected_wage
        job_response.save()
        return redirect('job_detail', job_offer.id)
    context['form'] = form

    return render(request, "createResponseForm.html", context)


def job_detail(request, job_id):
    job = JobOffer.objects.get(id=job_id)
    job_response_count = ResponsesCounter.objects.get(job_offer_id_id=job_id).count
    context = {'job': job, 'count': job_response_count}
    if request.user.is_authenticated & (request.user.id == job.user_id_id):
        context['show_button'] = True
    else:
        context['show_button'] = False

    return render(request, "jobOfferDetails.html", context)


def job_response_form(request, job_id):
    job_response = JobResponse.objects.get(job_offer_id_id=job_id)
    form_class = generate_response_form(job_response)
    mailer = Mailer()

    if request.method == 'POST':
        form = form_class(request.POST, request.FILES)
        # form.save(commit=False)
        if form.is_valid():
            try:
                email = (extract_value(form['email']))
            except:
                email = None
            try:
                cv = (request.FILES['cv'])
            except:
                cv = None
            try:
                letter_of_motivation = (request.FILES['letter_of_motivation'])
            except:
                letter_of_motivation = None
            try:
                expected_wage = (extract_value(form['expected_wage']))
            except:
                expected_wage = None
            if job_response.link:
                job_response_link = job_response.link
            else:
                job_response_link = "http://akneroth.ddns.net:9003/details/{}".format(job_id)

            if email:
                content = JobOfferResponse(cv_file_name=cv,
                                           motivation_letter_file_name=letter_of_motivation,
                                           expected_wage=expected_wage,
                                           job_offer_link=job_response_link)
                mailer.send_job_offer_response(recipient=email, content=content)

            counter = get_object_or_404(ResponsesCounter, job_offer_id_id=job_id)
            if counter:
                counter.count += 1
                counter.save(update_fields=['count'])
            else:
                ResponsesCounter.objects.create(job_offer_id_id=job_id, count=1)

            return redirect("job_detail", job_id)
    else:
        form = form_class()

    return render(request, 'jobOfferResponse.html', {'form': form})


def extract_value(input_html):
    value_regex = r'value="(.+?)"'
    match = re.search(value_regex, str(input_html))
    if match:
        value = match.group(1)
        return value


@login_required()
def edit_job_offer(request, job_id):
    job_offer = get_object_or_404(JobOffer, id=job_id)
    form = OfferForm(instance=job_offer)
    context = {'form': form}
    if request.method == "POST":
        form = OfferForm(request.POST, instance=job_offer)
        if form.is_valid():
            job_offer = form.save(commit=False)
            job_offer.expiration_date = form.cleaned_data['expiration_date']
            job_offer.save()
            return redirect('job_detail', job_offer.id)
    context['form'] = form

    return render(request, "jobOfferEdit.html", context)


def job_offer_list(request):
    job_offer_categories = JobOffer.CATEGORIES
    job_offer_contract_types = JobOffer.CONTRACT_TYPES
    job_offer_expected_education = JobOffer.EXPECTED_EDUCATION
    job_offer_operating_mode = JobOffer.OPERATING_MODE

    search_term = request.GET.get('search', '')
    category_filter = request.GET.get('category', '')
    contract_type_filter = request.GET.get('contract_type', '')
    expected_education_filter = request.GET.get('expected_education', '')
    operating_mode_filter = request.GET.get('operating_mode', '')

    job_offers = JobOffer.objects.all().filter(Q(expiration_date__gte=timezone.now()) | Q(expiration_date__isnull=True))

    if search_term:
        job_offers = job_offers.filter(Q(title__icontains=search_term) | Q(localization__icontains=search_term))

    if category_filter:  # new
        job_offers = job_offers.filter(category=category_filter)

    if contract_type_filter:
        job_offers = job_offers.filter(contract_type=contract_type_filter)

    if expected_education_filter:
        job_offers = job_offers.filter(expected_education=expected_education_filter)

    if operating_mode_filter:
        job_offers = job_offers.filter(operating_mode=operating_mode_filter)

    return render(request, 'jobOffersList.html',
                  {'job_offers': job_offers, 'job_offer_categories': job_offer_categories,
                   'job_offer_contract_types': job_offer_contract_types,
                   'job_offer_expected_education': job_offer_expected_education,
                   'job_offer_operating_mode': job_offer_operating_mode})


@login_required()
def manage_job_offers(request):
    job_offers = JobOffer.objects.filter(user_id_id=request.user.id)
    context = {'job_offers': job_offers}
    job_responses = {}
    for job in job_offers:
        job_responses[job.id] = ResponsesCounter.objects.get(job_offer_id_id=job.id).count
    context['job_responses'] = job_responses

    return render(request, "manage.html", context)


@login_required()
def delete_job_offer(request, job_id):
    job_offer = get_object_or_404(JobOffer, id=job_id)
    job_offer.delete()
    return redirect('manage_job_offers')

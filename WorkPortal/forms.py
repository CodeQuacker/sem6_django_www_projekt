import django.contrib.auth
from captcha.fields import CaptchaField
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import JobOffer, JobResponse, ResponsesCounter


class UserCreationForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user


class OfferForm(forms.ModelForm):
    class Meta:
        model = JobOffer
        fields = ['category', 'title', 'expiration_date', 'localization', 'description', 'rate', 'contract_type',
                  'expected_education', 'expected_age', 'required_experience', 'operating_mode', 'shift_work',
                  'weekend_work', 'flexible_working_time']
        widgets = {
            'expiration_date': forms.DateTimeInput(attrs={'type': 'datetime-local'}),
            'description': forms.Textarea(attrs={'rows': 5, 'cols': 30})
        }


def generate_response_form(model):
    fields = {}

    if model.email:
        fields['email'] = forms.EmailField()

    if model.cv:
        fields['cv'] = forms.FileField()

    if model.letter_of_motivation:
        fields['letter_of_motivation'] = forms.FileField()

    if model.expected_wage:
        fields['expected_wage'] = forms.DecimalField()

    fields['captcha'] = CaptchaField()
    return type('JobResponseForm', (forms.Form,), fields)


class ResponseForm(forms.ModelForm):
    class Meta:
        model = JobResponse
        fields = ['link', 'email', 'cv', 'letter_of_motivation', 'expected_wage']


class ResponsesCounterForm(forms.ModelForm):
    class Meta:
        model = ResponsesCounter
        fields = []

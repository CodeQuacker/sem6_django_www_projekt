from django import template

register = template.Library()


@register.filter(name='cut')
def cut(value, arg):
    """
    Removes all values of arg from the given string
    """
    return value.replace(arg, '')


@register.filter
def get_response_counter(job_responses, job_id):
    return job_responses[job_id]


@register.filter
def add_class(field, css_class):
    return field.as_widget(attrs={'class': css_class})


@register.filter(name='attr')
def attr(field, css):
    attrs = {}
    definition = css.split(',')
    for d in definition:
        t = d.split(':')
        attrs[t[0]] = t[1]
    return field.as_widget(attrs=attrs)

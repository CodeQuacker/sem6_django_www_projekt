from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path("", views.home, name="home"),
    # path('search/', views.search, name='search'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('register/', views.register, name='register'),
    path('createJobOffer/', views.create_job_offer, name='job_create'),
    path('createResponseForm/<int:job_offer_id>', views.create_response_form, name="create_response_form"),
    path('details/<int:job_id>/', views.job_detail, name='job_detail'),
    path('respond/<int:job_id>/', views.job_response_form, name='job_response'),
    path('edit/<int:job_id>/', views.edit_job_offer, name='edit_job_offer'),
    path('jobOffers/', views.job_offer_list, name='job_offer_list'),
    path('delete/<int:job_id>/', views.delete_job_offer, name='delete_job_offer'),
    path('manage/', views.manage_job_offers, name='manage_job_offers'),
    path('captcha/', include('captcha.urls')),
]

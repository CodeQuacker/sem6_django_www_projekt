import os
import textwrap

from mailjet_rest import Client
from dotenv import load_dotenv


class JobOfferResponse(object):
    def __init__(self, cv_file_name, motivation_letter_file_name, expected_wage, job_offer_link):
        self.cv_file_name = cv_file_name
        self.motivation_letter_file_name = motivation_letter_file_name
        self.expected_wage = expected_wage
        self.job_offer_link = job_offer_link

    def get_mail_content(self):
        mail_head = """
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="UTF-8">
                <title>Mailjet Response Email</title>
                <style>
                    body {
                        font-family: Arial, sans-serif;
                        font-size: 16px;
                        line-height: 1.5;
                        color: #333;
                        background-color: #f5f5f5;
                        padding: 20px;
                    }
                    h1, h2, h3, h4, h5, h6 {
                        font-weight: bold;
                        margin-top: 0;
                        margin-bottom: 20px;
                    }
                    p {
                        text-align: justify;
                        margin-bottom: 20px;
                    }
                    .link {
                        color: #007bff;
                        text-decoration: none;
                        font-weight: bold;
                    }
                </style>
            </head>"""
        mail_body_head = """
            <body>
                <p>Szanowny/a Kandydacie/Kandydatko,</p>
                <p>Dziękujemy za przesłanie swojej aplikacji do <a href="{job_offer_link}" class="link">tego ogłoszenia o pracę</a>. Otrzymaliśmy Twoje dokumenty aplikacyjne:</p>
                <ul>"""
        mail_list_cv = """
                    <li>CV: <strong>{cv_file_name}</strong></li>"""
        mail_list_mot_letter = """
                    <li>List motywacyjny: <strong>{motivation_letter_file_name}</strong></li>"""

        mail_list_end = """
                </ul>"""
        mail_body_wage = """
                <p>Twoje oczekiwania finansowe to: <strong>{expected_wage} zł brutto/miesiąc</strong>.</p>"""
        mail_body_tail = """ 
                <p>Prosimy o cierpliwość - wkrótce przeanalizujemy Twoją aplikację i postaramy się odpowiedzieć jak najszybciej.</p>
                <p>Pozdrawiamy serdecznie,</p>
                <h3>Zespół Rekrutacji</h3>
            </body>
            </html>"""

        mail_body = mail_body_head.format(job_offer_link=self.job_offer_link)
        if self.cv_file_name:
            mail_body += mail_list_cv.format(cv_file_name=self.cv_file_name)
        if self.motivation_letter_file_name:
            mail_body += mail_list_mot_letter.format(motivation_letter_file_name=self.motivation_letter_file_name)
        mail_body += mail_list_end
        if self.expected_wage:
            mail_body += mail_body_wage.format(expected_wage=self.expected_wage)
        mail_body += mail_body_tail

        mail = mail_head + mail_body
        return textwrap.dedent(mail)


# example use
# my_mailer = Mailer()
# my_mailer.send_job_offer_response(recipient="zalewskiadam220@gmail.com",
#                                   content=JobOfferResponse("cv_file.pdf", "mot_letter.pdf", 5000, "link.com"))
class Mailer:
    def __init__(self):
        load_dotenv()
        self.mailjet_sender_email = os.getenv("MAILJET_SENDER_EMAIL")
        self.mailjet_sender_name = os.getenv("MAILJET_SENDER_NAME")
        mailjet_api_key = os.getenv("MAILJET_API_KEY")
        mailjet_api_secret = os.getenv("MAILJET_API_SECRET")
        self.mailer = Client(auth=(mailjet_api_key, mailjet_api_secret), version='v3.1')

    def __send_email(self, recipient, subject, content):
        # Create mail
        message = {
            'From': {
                'Email': self.mailjet_sender_email,
                'Name': self.mailjet_sender_name
            },
            'To': [
                {
                    'Email': recipient,
                    'Name': recipient
                }
            ],
            'Subject': subject,
            'HTMLPart': content
        }

        # Send email
        mailer_response = self.mailer.send.create(data={'Messages': [message]})

        if mailer_response.status_code == 200:
            return True
        else:
            return False

    def send_job_offer_response(self, recipient, content: JobOfferResponse):
        return self.__send_email(recipient, "Dziękujemy za przesłanie aplikacji!", content.get_mail_content())

# Debug
# content = JobOfferResponse(cv_file_name=None,
#                            motivation_letter_file_name=None,
#                            expected_wage=None,
#                            job_offer_link="http://akneroth.ddns.net:9003/details/{}".format(3))
# print(content.get_mail_content())

from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(JobOffer)
admin.site.register(JobResponse)
admin.site.register(ResponsesCounter)
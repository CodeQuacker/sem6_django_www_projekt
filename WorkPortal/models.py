from django.contrib.auth.models import User
from django.db import models


# Create your models here.

class JobOffer(models.Model):
    CATEGORIES = [
        ("ADB", "Office administration"),
        ("ECO", "E-commerce"),
        ("FWF", "Franchising / Own company"),
        ("HRU", "Hostess, flyer distribution"),
        ("KIE", "Driver"),
        ("MIS", "Installation and service"),
        ("PZG", "Work abroad"),
        ("RIO", "Agriculture and horticulture"),
        ("ZDR", "Health"),
        ("KAK", "Management"),
        ("BIR", "Research and development"),
        ("EDU", "Education"),
        ("FRK", "Hairdressing, beauty treatments"),
        ("HOT", "Hotel industry"),
        ("LZS", "Logistics, purchasing, forwarding"),
        ("OCC", "Customer service and call center"),
        ("PRM", "Warehouse work"),
        ("SPR", "Cleaning"),
        ("POP", "Other job offers"),
        ("BUD", "Construction / renovations"),
        ("ENG", "Power industry"),
        ("GAS", "Catering"),
        ("INZ", "Engineering"),
        ("MPR", "Marketing and PR"),
        ("OCH", "Security"),
        ("PSK", "Shop assistant"),
        ("SPR", "Sales"),
        ("PDS", "Additional / seasonal work"),
        ("DKM", "Courier, city delivery"),
        ("FIK", "Finance / accounting"),
        ("HRR", "HR"),
        ("ITT", "IT / telecommunications"),
        ("MIL", "Mechanics and car painting"),
        ("OPI", "Care"),
        ("PRS", "Internships"),
        ("PRO", "Production"),
        ("WET", "Shelf-stocking and product presentation")
    ]

    CONTRACT_TYPES = [
        ("UOP", "Employment contract"),
        ("UOD", "Contract of mandate"),
        ("UZL", "Contract for specific task"),
        ("B2B", "B2B contract"),
        ("UNZ", "Substitution contract"),
        ("UAG", "Agency contract"),
        ("UPT", "Temporary employment contract"),
        ("USP", "Apprenticeship / internship contract")
    ]

    EXPECTED_EDUCATION = [
        ("POD", "Basic education"),
        ("GIM", "Secondary education"),
        ("WZZ", "Basic vocational education"),
        ("WZB", "Basic vocational education in a specific trade"),
        ("WSB", "Vocational education in a specific trade"),
        ("WSR", "Secondary education"),
        ("WWY", "Higher education")
    ]

    OPERATING_MODE = [
        ("PST", "Stationary work"),
        ("PZD", "Remote work"),
        ("PHY", "Hybrid work"),
        ("PMO", "Mobile work")
    ]

    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    category = models.CharField(max_length=3, choices=CATEGORIES)
    title = models.CharField(max_length=256)
    expiration_date = models.DateTimeField(null=True, blank=True)
    localization = models.CharField(max_length=256, null=True, blank=True)
    description = models.CharField(max_length=4096)
    rate = models.IntegerField(null=True, blank=True)
    contract_type = models.CharField(max_length=3, choices=CONTRACT_TYPES)
    expected_education = models.CharField(max_length=3, choices=EXPECTED_EDUCATION, null=True, blank=True)
    expected_age = models.CharField(max_length=16, null=True, blank=True)
    required_experience = models.IntegerField(null=True, blank=True)
    operating_mode = models.CharField(max_length=3, choices=OPERATING_MODE, null=True, blank=True)
    shift_work = models.BooleanField()
    weekend_work = models.BooleanField()
    flexible_working_time = models.BooleanField()


class JobResponse(models.Model):
    job_offer_id = models.ForeignKey(JobOffer, on_delete=models.CASCADE)
    link = models.CharField(max_length=512, null=True, blank=True)
    email = models.BooleanField()
    cv = models.BooleanField()
    letter_of_motivation = models.BooleanField()
    expected_wage = models.BooleanField()


class ResponsesCounter(models.Model):
    job_offer_id = models.ForeignKey(JobOffer, on_delete=models.CASCADE)
    count = models.IntegerField()
